# SurrogateModelOptim

[![Stable](https://img.shields.io/badge/docs-stable-blue.svg)](https://MrUrq.github.io/SurrogateModelOptim.jl/stable)
[![Latest](https://img.shields.io/badge/docs-latest-blue.svg)](https://MrUrq.github.io/SurrogateModelOptim.jl/latest)
[![Build Status](https://travis-ci.org/MrUrq/SurrogateModelOptim.jl.svg?branch=master)](https://travis-ci.org/MrUrq/SurrogateModelOptim.jl)
[![Build Status](https://ci.appveyor.com/api/projects/status/github/MrUrq/SurrogateModelOptim.jl?svg=true)](https://ci.appveyor.com/project/MrUrq/SurrogateModelOptim-jl)
[![Codecov](https://codecov.io/gh/MrUrq/SurrogateModelOptim.jl/branch/master/graph/badge.svg)](https://codecov.io/gh/MrUrq/SurrogateModelOptim.jl)
[![Coveralls](https://coveralls.io/repos/github/MrUrq/SurrogateModelOptim.jl/badge.svg?branch=master)](https://coveralls.io/github/MrUrq/SurrogateModelOptim.jl?branch=master)
[![Project Status: Concept – Minimal or no implementation has been done yet, or the repository is only intended to be a limited example, demo, or proof-of-concept.](https://www.repostatus.org/badges/latest/concept.svg)](https://www.repostatus.org/#concept)
